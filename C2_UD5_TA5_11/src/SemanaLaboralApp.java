import javax.swing.JOptionPane;

public class SemanaLaboralApp {

	public static void main(String[] args) {
		
		// Declaraci�n de un switch para comprobar si el string introducido es un d�a de la semana
		String diaSemana = JOptionPane.showInputDialog("Introduce un d�a de la semana");
		
		switch (diaSemana) {
			
			case "Lunes":
				JOptionPane.showMessageDialog(null, "Hoy es " + diaSemana);
				break;
				
			case "Martes":
				JOptionPane.showMessageDialog(null, "Hoy es " + diaSemana);
				break;
			
			case "Miercoles":
				JOptionPane.showMessageDialog(null, "Hoy es " + diaSemana);
				break;
				
			case "Jueves":
				JOptionPane.showMessageDialog(null, "Hoy es " + diaSemana);
				break;
				
			case "Viernes":
				JOptionPane.showMessageDialog(null, "Hoy es " + diaSemana);
				break;
				
			case "Sabado":
				JOptionPane.showMessageDialog(null, "Hoy es " + diaSemana);
				break;
				
			case "Domingo":
				JOptionPane.showMessageDialog(null, "Hoy es " + diaSemana);
				break;
			
			default:
				JOptionPane.showMessageDialog(null, "No has introducido un dia correcto");
		
		}

	}

}
