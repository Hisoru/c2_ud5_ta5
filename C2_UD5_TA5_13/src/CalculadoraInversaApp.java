import javax.swing.JOptionPane;

public class CalculadoraInversaApp {

	public static void main(String[] args) {
		
		// Petici�n de dos n�meros y un signo aritm�tico
		String primerNumero = JOptionPane.showInputDialog("Introduce un primer n�mero");
		String segundoNumero = JOptionPane.showInputDialog("Introduce un segundo n�mero");
		String signo = JOptionPane.showInputDialog("Introduce un signo aritm�tico");
		
		// Conversi�n de los strings recibidos a int
		int primerNumeroInt = Integer.parseInt(primerNumero);
		int segundoNumeroInt = Integer.parseInt(segundoNumero);
		
		// Declaraci�n de un switch para hacer el c�lculo de los dos n�meros recibidos con el signo aritm�tico correspondiente
		switch (signo) {
		
			case "+":
				JOptionPane.showMessageDialog(null, primerNumeroInt + segundoNumeroInt);
				break;
				
			case "-":
				JOptionPane.showMessageDialog(null, primerNumeroInt - segundoNumeroInt);
				break;
			
			case "*":
				JOptionPane.showMessageDialog(null, primerNumeroInt * segundoNumeroInt);
				break;
				
			case "/":
				JOptionPane.showMessageDialog(null, primerNumeroInt / segundoNumeroInt);
				break;
				
			case "^":
				JOptionPane.showMessageDialog(null, primerNumeroInt ^ segundoNumeroInt);
				break;
				
			case "%":
				JOptionPane.showMessageDialog(null, primerNumeroInt % segundoNumeroInt);
				break;
			
			default:
				JOptionPane.showMessageDialog(null, "No has introducido un signo aritm�tico correcto");
	
		}

	}

}
