
import javax.swing.JOptionPane;

public class MensajeBienvenidaInputApp {

	public static void main(String[] args) {
		
		// Declaración de un string
		String nombre = JOptionPane.showInputDialog("Introduce tu nombre");
				
		// Muestra por consola del string
		System.out.println("Bienvenido " + nombre);

	}

}
