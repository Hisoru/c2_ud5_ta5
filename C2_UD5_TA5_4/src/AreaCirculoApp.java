import javax.swing.JOptionPane;

public class AreaCirculoApp {

	public static void main(String[] args) {
		
		// Petici�n de radio por teclado
		String radio = JOptionPane.showInputDialog("Introduce un valor");
		
		// Conversi�n de string a double del radio y c�lculo del �rea
		System.out.println(Math.PI * Math.pow(Double.parseDouble(radio), 2));

	}

}
