

public class ComparacionMayorApp {

	public static void main(String[] args) {
		
		// Declaraci�n de dos variables
		int A = 1;
		int B = 2;
		
		// Comparacion de las variables
		if (A > B) {
			
			System.out.println("El n�mero " + A + " es mayor que " + B);
			
		} else if (B > A) {
			
			System.out.println("El n�mero " + B + " es mayor que " + A);
			
		} else {
			
			System.out.println("Los dos n�meros son iguales");
			
		}

	}

}
