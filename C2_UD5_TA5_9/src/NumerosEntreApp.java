
public class NumerosEntreApp {

	public static void main(String[] args) {
		
		// Declaraci�n de un bucle for para mostrar los n�meros del 1 al 100
		for (int i = 1 ; i <= 100 ; i++) {
							
			if (i % 2 == 0 && i % 3 == 0 ) {
				
				System.out.println(i);
				
			}
							
		}

	}

}
