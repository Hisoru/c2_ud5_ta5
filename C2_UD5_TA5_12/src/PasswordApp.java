import javax.swing.JOptionPane;

public class PasswordApp {

	public static void main(String[] args) {
		
		// Declaraci�n de un string con una contrase�a, un boolean que permitir� la salida del bucle y un int que servir� de contador
		String password = "fundacioEsplai2020";
		boolean acierto = false;
		int intento = 0;
		
		// Declaraci�n de un bucle do/while que comprueba si la contrase�a introducida coincide con la declarada en la variable password
		do {
			
			String passwordInput = JOptionPane.showInputDialog("Introduce la contrase�a");
			
			if (passwordInput.equals(password)) {
				
				acierto = true;
				JOptionPane.showMessageDialog(null, "Enhorabuena");
				
			}
			
			intento++;
			
		} while (acierto == false && intento < 3);
		

	}

}
