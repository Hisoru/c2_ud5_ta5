import javax.swing.JOptionPane;

public class NumeroDivisibleApp {

	public static void main(String[] args) {
		
		// Petici�n de un n�mero
		String numero = JOptionPane.showInputDialog("Introduce un n�mero");
		
		// C�lculo de la divisi�n del n�mero entre dos
		if (Integer.parseInt(numero) % 2 == 0) {
			
			System.out.println("El n�mero introducido es divisible entre 2");
			
		} else {
			
			System.out.println("El n�mero introducido no es divisible entre 2");
			
		}

	}

}
