
public class NumerosWhileApp {

	public static void main(String[] args) {
		
		// Declaración de una variable
		int a = 1;
		
		// Declaración de un bucle while para mostrar los números del 1 al 100
		while (a <= 100) {
			
			System.out.println(a);
			a++;
			
		}

	}

}
