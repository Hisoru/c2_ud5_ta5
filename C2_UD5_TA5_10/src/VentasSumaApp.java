import javax.swing.JOptionPane;

public class VentasSumaApp {

	public static void main(String[] args) {
		
		// Petici�n del n�mero de ventas a sumar y declaraci�n de una variable para sumarlas
		String ventas = JOptionPane.showInputDialog("Introduce el n�mero de ventas a sumar");
		double suma = 0;
		
		// Declaraci�n de un bucle for que pregunta los precios de las ventas y los suma en una variable
		for (int i = 0 ; i <= Integer.parseInt(ventas) ; i++) {
			
			String precio = JOptionPane.showInputDialog("Introduce un precio");
			suma += Double.parseDouble(precio);
			
		}
		
		// Muestra en una caja de texto de la suma de las ventas
		JOptionPane.showMessageDialog(null, "La suma de todas las ventas es de " + suma);

	}

}
