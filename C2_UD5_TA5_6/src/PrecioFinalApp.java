import javax.swing.JOptionPane;

public class PrecioFinalApp {

	public static void main(String[] args) {
		
		// Petici�n del precio de un producto y declaraci�n de una variable con el IVA
		String precio = JOptionPane.showInputDialog("Introduce el precio del producto");
		final double IVA = 0.21;
		
		// Muestra por caja de texto del precio final
		JOptionPane.showMessageDialog(null, "El producto tiene un precio de " + (Double.parseDouble(precio) + (Double.parseDouble(precio) * IVA)));

	}

}
